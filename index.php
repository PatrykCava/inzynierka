<!DOCTYPE html>
<?php
require ('php/config/config.php');
require ('./php/classes/Connection.php');
require ('./php/classes/User.php');
$user = new User();

header('Content-Type: text/html; charset=utf-8');
if (empty($_GET['page'])) {
    $url = 'home';
} else {
    $url = $_GET['page'];
}

echo 'get <br>';
print_r($_GET);

echo '<br>post<br>';
print_r($_POST);
echo '<br>sesja<br>';
print_r($_SESSION);

if (file_exists('./php/logic/' . $url . 'pageLogic.php')) {
    include './php/logic/' . $url . 'pageLogic.php';
}
?>

<html>
    <head>
        <?php
        require ('views/_css_js.php');
        ?>
    </head>
    <body>
        <header>
            <div class="container-fluid">
                <img class="center-block banner" src="img/baner2.jpg"onclick ="$('#myModal').modal()"/>
            </div>
        </header>      
        <?php
        require ('./views/_nav.php');
        ?>
        <main class="container" id="main">
            <?php
            include ('./views/pages/' . $url . 'page.php');
            ?>
        </main>
        <?php
        require ('./views/_footer.php');
        include ('./views/_modals.php');
        ?>
    </body>
</html>
