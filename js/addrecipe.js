var used_ingredients = [];
var ingredients = [];
for_use = [];
var units = [];
ajaxGetIngredient();
ajaxGetUnits();
$(document).ready(function () {
    console.log(ingredients);
    var max_ingredient = ingredients.length;
    var wrap = $('.ingredient');
    var add_button = $('#add_next_ingredient');
    var x = 1;
    $(add_button).click(function (e) {

        var unitString = optionUnitsString();
        e.preventDefault();
        if (x < max_ingredient) {
            x++;
            $(wrap).append('<div class="row"><div class="col-xs-12 m-b-10"><div class="col-xs-3"><label>Ile?</label><input class="how_much form-control" type="number" name="howMuch[]" value="1" step="0.1" min="0.1" onchange="checkValue()"></div><div class="col-xs-3"><label>Miarka: </label><select class="form-control type_select" name="type[]" onchange="insertIngrediant(' + x + ')"">' + unitString + '</select></div><div class="col-xs-3"><label>Jaki?</label><select id="ingredient_select_' + x + '" class="form-control ingredient_select" name="ingredient[]"><script>firstFill('+x+')</script></select></div><div class="col-xs-3"><label></label><button href="#" class="delete form-control">Usuń</button></div></div></div>');
        } else
        {
            alert('Wiecęj dodać nie można');
        }
    });

    $(wrap).on("click", ".delete", function (e) {
        e.preventDefault();
        $(this).parent('div').parent('div').parent('div').remove();
        x--;
    });
});



function insertIngrediant(x) {
    var unitsArray = [];
    $(".type_select").each(function () {
        unitsArray.push(this.value);
    });
    var unitID = unitsArray.slice(-1);

    switch (parseInt(unitID)) {
        case 1:
            for_use = ['mąki', 'wody', 'masła', 'proszku_do_pieczenia', 'cukru', 'oleju'];
            break;
        case 2:
            for_use = ['mąki', 'wody', 'masła', 'proszku_do_pieczenia', 'cukru', 'oleju'];
            break;
        case 3:
            for_use = ['mąki', 'wody', 'proszku_do_pieczenia', 'cukru'];
            break;
        case 4:
            for_use = ['mąki', 'wody', 'cukru', 'oleju'];
            break;
        case 5:
            for_use = ['mąki', 'wody', 'cukru', 'oleju'];
            break;
        case 6:
            for_use = ['jajko'];
            break;
        case 7:
            for_use = ['mąki', 'masła', 'cukru'];
            break;
        case 8:
            for_use = ['wody', 'oleju'];
            break;
        case 9:
            for_use = ['mąki', 'masła', 'proszku_do_pieczenia', 'cukru'];
            break;
        case 10:
            for_use = ['mąki', 'masła', 'proszku_do_pieczenia', 'cukru'];
            break;
        case 11:
            for_use = ['wody', 'oleju'];
            break;
        case 12:
            for_use = ['mąki', 'masła', 'proszku_do_pieczenia'];
            break;
    }
    fillSelect(x, for_use);
}

function firstFill(x) {
    for_use = ['mąki', 'wody', 'masła', 'proszku_do_pieczenia', 'cukru', 'oleju'];
    fillSelect(x, for_use);
}

function fillSelect(x, for_use) {
    used_ingredients = getUsedIgredients();
    createOptionValues(for_use);
    var optionIgredient = optionIngredientsString();
    $('#ingredient_select_' + x).html(optionIgredient);
}

function ajaxGetIngredient() {
    $.ajax({
        url: "php/ajax/getingredients.php",
        contentType: false,
        processData: false,
        success: function (result) {
            json = JSON.parse(result);
            for (var i = 0; i < json.length; i++) {
                ingredients[i] = json[i]['nazwa_skladnika'];
            }
            return ingredients;
        }
    });
}

function getUsedIgredients() {
    used_ingredients = [];
    $(".ingredient_select").each(function () {
        used_ingredients.push(this.value);
    });
    return used_ingredients;
}

function createOptionValues(for_use) {
    for (var i = 0; i < used_ingredients.length; i++) {
        var index = (for_use.indexOf(used_ingredients[i]));
        if (index > -1) {
            for_use.splice(index, 1);
        }
    }
    return for_use;
}

function optionIngredientsString() {
    var options = "";
    for (var i = 0; i < for_use.length; i++) {
        options += '<option value="' + for_use[i] + '">' + for_use[i] + '</option>';
    }
    return options;
}

function ajaxGetUnits() {
    $.ajax({
        url: "php/ajax/getunits.php",
        contentType: false,
        processData: false,
        success: function (result) {
            json = JSON.parse(result);
            for (var i = 0; i < json.length; i++) {
                var temp = [json[i]['id_jednostka'], json[i]['nazwa_jednostki']];
                units.push(temp);
            }
            return units;
        }
    });
}

function optionUnitsString() {
    var option = "";
    for (var i = 0; i < units.length; i++) {
        option += '<option value="' + units[i][0] + '">' + units[i][1] + '</option>';
    }
    return option;
}
/*
 * testy
 */
function addRecipe() {
    var form = document.getElementById("new_recipe");
    var formdata = new FormData(form);
    $.ajax({
        url: "php/ajax/add_new_recipe.php",
        type: "POST",
        data: formdata,
        contentType: false,
        processData: false,
        mimeType: 'multipart/form-data',
        success: function (result) {
            console.log(result);
        }
    });
}
//do zrobienia
function checkValue() {
    $('.how_much').each(function () {
        if ($(this).val() < 0.1) {
            $('#modal-wrong-value').modal();
        }
    });
}

function correctInput(){
    $('.how_much').each(function () {
        if ($(this).val() < 0.1) {
            $(this).val(0.1);
        }
    });
    $('#modal-wrong-value').hide();
}