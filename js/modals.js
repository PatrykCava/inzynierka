function logOutWindow() {
    $("#modal-logout").modal();
}
function logOut() {
    $.ajax({
        url: "php/ajax/logout.php",
        type: "POST",
        contentType: false,
        processData: false,
        success: function (result) {
            console.log(result);
            $("#modal-logout").modal("hide");
            window.location.href = 'index.php';
        }
    });
}