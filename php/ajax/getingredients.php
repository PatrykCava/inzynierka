<?php


require '../config/config.php';
require '../classes/Recipe.php';
require '../classes/Connection.php';

$recipe = new Recipe();
$ingregients = $recipe->getIngredients();
echo json_encode($ingregients);