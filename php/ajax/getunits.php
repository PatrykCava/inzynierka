<?php

require '../config/config.php';
require '../classes/Recipe.php';
require '../classes/Connection.php';

$recipe = new Recipe();
$units = $recipe->getUnits();
echo json_encode($units);
