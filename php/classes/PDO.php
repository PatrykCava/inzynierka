<?php

class PDO{
    public $connect = false;
    private $db_connection = null;
    public $errors = array();
    public $messages = array();

    private function databaseConnection() {
        if ($this->db_connection != NULL) {
            return true;
        } else {
            try {
                $this->db_connection = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';', DB_USER, DB_PASS);
                $connect = true;
            } catch (PDOException $e) {
                $this->errors[] = MESSAGE_DATABASE_ERROR . $e->getMessage();
            }
        }
        return false;
    }
    
        
    public function registration($imie, $nazwisko, $login, $haslo, $telefon, $email) {
        if ($this->databaseConnection()) {
            $query = $this->db_connection->prepare("INSERT INTO `uzytkownicy`(`imie`, `nazwisko`, `login`, `haslo`, `telefon`, `e-mail`) VALUES (:imie,:nazwisko,:login,:haslo,:telefon,:email);");
            $query->bindValue(":imie", $imie, PDO::PARAM_STR);
            $query->bindValue(":nazwisko", $nazwisko, PDO::PARAM_STR);
            $query->bindValue(":login", $login, PDO::PARAM_STR);
            $query->bindValue(":haslo", $haslo, PDO::PARAM_STR);
            $query->bindValue(":telefon", $telefon, PDO::PARAM_INT);
            $query->bindValue(":email", $email, PDO::PARAM_STR);
            $query->execute();
            return true;
        } else {
            $this->errors[] = "Wystapil blad polaczenia sie z baza";
            return false;
        }
    }

    public function logIn($login, $haslo) {
        if ($this->databaseConnection()) {
            
            $haslo_hash = hash("sha256", $haslo);
            
            $query = $this->db_connection->prepare("SELECT * FROM users WHERE login=:login AND haslo=:haslo");
            $query->bindValue(":login", $login, PDO::PARAM_STR);
            $query->bindValue(":haslo", $haslo_hash, PDO::PARAM_STR);
            $query->execute();
            $wynik = $query->fetchAll();
            if (count($wynik) > 0) {
                $_SESSION['ID_user'] = $wynik[0]['ID_user'];
                $_SESSION['log'] = 1;
                return true;
            } else {
                $this->errors[] = "Nie udalo sie zalogowac";
                echo "false";
            }
        }
    }

    public function checkLogIn() {
        if (isset($_SESSION['log']) && $_SESSION['log'] == 1) {
            return true;
        } else {
            return false;
        }
    }
    
    public function logOut(){
        $_SESSION['log']=0;
        unset($_SESSION);
       return true;
    }
    

}