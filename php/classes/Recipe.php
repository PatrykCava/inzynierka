<?php

class Recipe {

    public $DB = null;

    public function __construct() {
        if ($this->DB == null) {
            $this->DB = new Connection();
        }
    }

    public function getRecipesPage($nr, $limit) {
        if ($this->DB->databaseConnection()) {
            $query = $this->DB->db_connection->prepare("SELECT * FROM przepisy LIMIT " . $nr . "," . $limit);
            $query->execute();
            $result = $query->fetchAll();
            return $result;
        } else {
            $this->errors[] = "Blad zapytania do listy przepisow, blad polacenia";
            return false;
        }
    }

    public function getRecipes() {
        if ($this->DB->databaseConnection()) {
            $query = $this->DB->db_connection->prepare("SELECT * FROM przepisy");
            $query->execute();
            $result = $query->fetchAll();
            return $result;
        } else {
            $this->errors[] = "Blad polaczenia";
            return false;
        }
    }

    public function getRecipe($idRecipe) {
        if ($this->DB->databaseConnection()) {
            $checkPhoto = $this->DB->db_connection->prepare("SELECT id_zdjecia_przepisu FROM przepisy WHERE id_przepisu = :id_przepisu");
            $checkPhoto->bindValue(":id_przepisu", $idRecipe, PDO::PARAM_INT);
            $checkPhoto->execute();
            $array = $checkPhoto->fetchAll();
            if (!empty($array[0]['id_zdjecia_przepisu'])) {
                $string = "SELECT * FROM `przepis_skladnik` AS p_s 
INNER JOIN `przepisy` AS p ON p_s.id_przepisu=p.id_przepisu
INNER JOIN `skladniki` AS s ON p_s.id_skladnik=s.id_skladnik
INNER JOIN `jednostki` As j ON s.id_jednostka=j.id_jednostka
INNER JOIN `zdjecia_przepisu` AS zdj_p ON p.id_przepisu=zdj_p.id_przepisu
INNER JOIN `zdjecia` AS zdj ON zdj_p.id_zdjecia=zdj.id_zdjecia
WHERE p.id_przepisu = :id_przepisu";
            } else {
                $string = "SELECT * FROM `przepis_skladnik` AS p_s 
INNER JOIN `przepisy` AS p ON p_s.id_przepisu=p.id_przepisu
INNER JOIN `skladniki` AS s ON p_s.id_skladnik=s.id_skladnik
INNER JOIN `jednostki` As j ON s.id_jednostka=j.id_jednostka
WHERE p.id_przepisu = :id_przepisu";
            }
            $query = $this->DB->db_connection->prepare($string);
            $query->bindValue(":id_przepisu", $idRecipe, PDO::PARAM_INT);
            $query->execute();
            $result = $query->fetchAll();
            return $result;
        } else {
            $this->errors[] = "Nie udalo sie znaleść przepisu";
            return false;
        }
    }

    public function searchRecipe($recipeName) {
        $recipeName = '%' . $recipeName . '%';
        if ($this->DB->databaseConnection()) {
            $query = $this->DB->db_connection->prepare("SELECT * FROM `przepisy` WHERE nazwa_przepisu LIKE :recipeName");
            $query->bindValue(":recipeName", $recipeName, PDO::PARAM_STR);
            $query->execute();
            $result = $query->fetchAll();
            if (count($result) > 0) {
                return $result;
            } else {
                return array();
            }
        } else {
            $this->errors[] = "Nie udalo sie znaleść przepisu";
            return false;
        }
    }

    public function getMainImg($id_recipe) {
        if ($this->DB->databaseConnection()) {
            $query = $this->DB->db_connection->prepare("SELECT zdjecia.lokalizacja_zdjecia FROM `przepisy` 
INNER JOIN `zdjecia_przepisu` ON przepisy.id_przepisu=zdjecia_przepisu.id_przepisu
INNER JOIN `zdjecia` ON zdjecia_przepisu.id_zdjecia=zdjecia.id_zdjecia
WHERE zdjecia.zdjecie_glowne =1 AND przepisy.id_przepisu=:id_recipe");
            $query->bindValue(":id_recipe", $id_recipe, PDO::PARAM_INT);
            $query->execute();
            $result = $query->fetchAll();
            if (count($result) > 0) {
                return $result;
            } else {
                return array();
            }
        } else {
            $this->errors[] = "";
            return false;
        }
    }
    
    public function getIngredients(){
        if ($this->DB->databaseConnection()) {
            $query = $this->DB->db_connection->prepare("SELECT DISTINCT nazwa_skladnika FROM `skladniki`");
            $query->execute();
            $result = $query->fetchAll();
            if (count($result) > 0) {
                return $result;
            } else {
                return array();
            }
        } else {
            $this->errors[] = "";
            return false;
        }
    }

    public function getUnits(){
        if ($this->DB->databaseConnection()) {
            $query = $this->DB->db_connection->prepare("SELECT `id_jednostka`,`nazwa_jednostki` FROM `jednostki`");
            $query->execute();
            $result = $query->fetchAll();
            if (count($result) > 0) {
                return $result;
            } else {
                return array();
            }
        } else {
            $this->errors[] = "";
            return false;
        }
    }
    
    public function addRecipe(){
        if ($this->DB->databaseConnection()) {
            $query = $this->DB->db_connection->prepare("SELECT `id_jednostka`,`nazwa_jednostki` FROM `jednostki`");
            $query->execute();
            
        } else {
            $this->errors[] = "";
            return false;
        }
    }
}
