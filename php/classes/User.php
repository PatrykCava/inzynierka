<?php

class User {

    public $DB = null;

    public function __construct() {
        session_start();
        if ($this->DB == null) {
            $this->DB = new Connection();
        }
    }

    public function registration($imie, $nazwisko, $login, $haslo, $powtHaslo, $email, $accept) {
        if ($this->DB->databaseConnection()) {
            $this->DB->db_connection->beginTransaction();
            try {
                $haslo = hash("sha256", $haslo);
                $powtHaslo = hash("sha256", $powtHaslo);
                if (($accept == 'on') && ($haslo == $powtHaslo)) {
                    $insHaslo = $this->DB->db_connection->prepare("INSERT INTO `hasla`(`haslo`) VALUES (:haslo);");
                    $insHaslo->bindValue(":haslo", $haslo, PDO::PARAM_STR);
                    $insHaslo->execute();

                    $id_haslo = $this->DB->db_connection->lastInsertID();
                    $insLogin = $this->DB->db_connection->prepare("INSERT INTO `loginy`(`id_hasla`,`login`) VALUES (:id_haslo,:login);");
                    $insLogin->bindValue(":id_haslo", $id_haslo, PDO::PARAM_INT);
                    $insLogin->bindValue(":login", $login, PDO::PARAM_STR);
                    $insLogin->execute();

                    $data_utworzenia = date('Y-m-d H:i:s');
                    $data_modyfikacji = $data_utworzenia;
                    $id_login = $this->DB->db_connection->lastInsertID();
                    $insUser = $this->DB->db_connection->prepare("INSERT INTO `uzytkownicy` (`imie`, `nazwisko`, `email`, `id_grupy_uzytkownikow`, `data_utworzenia`, `data_aktualizacji`, `id_login`) VALUES (:imie,:nazwisko,:email,:id_grupy,:data_utworzenia,:data_aktualizacji,:id_login);");
                    $insUser->bindValue(":imie", $imie, PDO::PARAM_STR);
                    $insUser->bindValue(":nazwisko", $nazwisko, PDO::PARAM_STR);
                    $insUser->bindValue(":email", $email, PDO::PARAM_STR);
                    $insUser->bindValue(":id_grupy", 1, PDO::PARAM_STR);
                    $insUser->bindValue(":data_utworzenia", $data_utworzenia, PDO::PARAM_STR);
                    $insUser->bindValue(":data_aktualizacji", $data_modyfikacji, PDO::PARAM_STR);
                    $insUser->bindValue(":id_login", $id_login, PDO::PARAM_INT);
                    $insUser->execute();

                    $id_user = $this->DB->db_connection->lastInsertID();
                    $insGroup = $this->DB->db_connection->prepare("INSERT INTO `grupy_uzytkownikow` (`id_grupy`,`id_uzytkownika`,`id_upowaznienia`) VALUES (1,:id_uzytkownika,1);");
                    $insGroup->bindValue(":id_uzytkownika", $id_user, PDO::PARAM_INT);
                    $insGroup->execute();
                    $this->DB->db_connection->commit();
                }
            } catch (Exception $e) {
                echo $e->getMessage();
                $this->DB->db_connection->rollback();
            }
            return true;
        } else {
            $this->errors[] = "Wystapil blad polaczenia sie z baza";
            return false;
        }
    }

    public function logIn($login, $haslo) {
        if ($this->DB->databaseConnection()) {
            echo 'polaczylo';
            $haslo_hash = hash("sha256", $haslo);
            $query = $this->DB->db_connection->prepare("SELECT * FROM uzytkownicy JOIN loginy ON uzytkownicy.id_login=loginy.id_login JOIN hasla ON loginy.id_hasla=hasla.id_hasla WHERE loginy.login=:login AND hasla.haslo=:haslo");
            $query->bindValue(":login", $login, PDO::PARAM_STR);
            $query->bindValue(":haslo", $haslo_hash, PDO::PARAM_STR);
            $query->execute();
            $wynik = $query->fetchAll();
            if (count($wynik) > 0) {
                $_SESSION['id_uzytkownika'] = $wynik[0]['id_uzytkownika'];
                $_SESSION['log'] = 1;
                header('Location: index.php?page=userpanel');
            } else {
                $this->errors[] = "Nie udalo sie zalogowac";
                echo false;
            }
        }
    }

    public function checkLogIn() {
        if (isset($_SESSION['log']) && $_SESSION['log'] != 0) {
            return true;
        } else {
            return false;
        }
    }

    public function logOut() {
        $_SESSION['log'] = 0;
        session_destroy();
        return true;
    }

    public function test() {
        echo 'jestem sobie user';
    }

}
