<?php

if (filter_has_var(INPUT_POST, 'log_in')) {
    $user->logIn(filter_input(INPUT_POST, 'login'), filter_input(INPUT_POST, 'password'));
}
if (filter_has_var(INPUT_POST, 'create_account')) {
    $user->registration(filter_input(INPUT_POST, 'first_name'), filter_input(INPUT_POST, 'last_name'), filter_input(INPUT_POST, 'loginRegister'), filter_input(INPUT_POST, 'passwordRegister'), filter_input(INPUT_POST, 'repeatPasswordRegister'), filter_input(INPUT_POST, 'email'), filter_input(INPUT_POST, 'acceptRulez'));
}
if (filter_has_var(INPUT_POST, 'modalLogOut')) {
    $user->logOut();
}