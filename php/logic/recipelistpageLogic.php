<?php

include './php/classes/Recipe.php';
$recipe = new Recipe();

function getImgSrc($id_recipe){
    $recipe = new Recipe();
    $src = $recipe->getMainImg($id_recipe);
    if(empty($src)){
        return '';
    } else {
        return $src[0]['lokalizacja_zdjecia'];
    }
}

if (empty($_POST['limitpage']) || $_POST['limitpage'] == "") {
    if (empty($_SESSION['limitpage']) || $_SESSION['limitpage'] == "") {
        $limit = 5;
    } else {
        $limit = $_SESSION['limitpage'];
    }
}

//sprawdanie limitu
if (filter_has_var(INPUT_POST, 'accChangesRecipe')) {
    $limit = $_POST['limitpage'];
    $_SESSION['limitpage'] = $limit;
    $_GET['nrpage'] = 1;
}


//sprawdzenie nr strony
if (empty($_GET['nrpage']) || $_GET['nrpage'] == "") {
    $page = 1;
} else {
    $page = $_GET['nrpage'];
}


$select = array(5, 10, 25, 50, 100);

if (filter_has_var(INPUT_POST, 'all_recipe')) {
    header('Location: index.php?page=recipelist');
    $limit = $_POST['limitpage'];
    $allRecipe = $recipe->getRecipesPage(($page * $limit) - $limit, $limit);
    $numberOfPages = ceil(count($recipe->getRecipes()) / $limit);
} elseif (empty($_POST['searchrecipe']) || $_POST['searchrecipe'] == "") {
    $allRecipe = $recipe->getRecipesPage(($page * $limit) - $limit, $limit);
    $numberOfPages = ceil(count($recipe->getRecipes()) / $limit);
} else {
    $allRecipe = $recipe->searchRecipe($_POST['searchrecipe']);
    $numberOfPages = 1;
}

if (filter_has_var(INPUT_POST, 'more_detail')) {
    $id = filter_input(INPUT_POST, 'more_detail');
    header('Location: index.php?page=recipe&id_przepisu=' . $id);
}