<?php
include './php/classes/Recipe.php';
$recipe = new Recipe();
$recipeInfo = $recipe->getRecipe($_GET['id_przepisu']);

function getIngredients($recipeInfo) {
    //dziala
    $temp = array();
    foreach ($recipeInfo as $info) {

        $toView = $info['ilosc'] . ' ' . $info['nazwa_jednostki'] . ' ' . $info['nazwa_skladnika'];
        if (in_array($toView, $temp)) {
            continue;
        } else {
            array_push($temp, $toView);
            ?>
            <li>
                <p>
                    <?php
                    echo $toView;
                    ?> 
                </p>
            </li>
            <?php
        }
    }
    return $temp;
}

function getImages($recipeInfo){
    $images = array();
    for ($i = 0; $i < count($recipeInfo); $i++) {
            if (isset($recipeInfo[$i]['lokalizacja_zdjecia'])) {
                $imgSrc = $recipeInfo[$i]['lokalizacja_zdjecia'];
                if (in_array($imgSrc, $images)) {
                    continue;
                } else {
                    array_push($images, $imgSrc);
                }
            }
        }
        return $images;
}
