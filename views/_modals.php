<!-- Modal wylogowania -->

<div class="modal fade in" data-backdrop="static" id="modal-logout" 
     tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog" role="document">
        <form method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <label>Wyloguj sie</label>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <label>Czy napewno chcesz wylogowac sie?</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Nie</button>
                        <div class="btn btn-danger" name="modalLogOut" onclick="logOut()">Tak</div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<!-- Modal wylogowania -->

<div id="modal-wrong-value" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <label>Błąd</label>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <label>Wprowadzona wartość jest niepoprawna - musi być więcej niż 0,1</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="footer">
                        <button type="button" class="btn btn-info" data-dismiss="modal" onclick="correctInput()">OK</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

