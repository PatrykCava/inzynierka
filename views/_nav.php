
<nav class="container-fluid">
    <div class="navbar">
        <form method="post">
            <ul class="navigation">

                <li class="navbutton"><a href="index.php?page=home">Strona główna</a></li>
                <li class="navbutton"><a href="index.php?page=recipelist" >Przepisy</a></li>
                <li class="navbutton"><a href="index.php?page=search">Szukaj</a></li>
                <li class="navbutton"><a href="index.php?page=contact">Kontakt</a></li>
                <?php
                if ($user->checkLogIn()) {
                    ?>
                <li class="navbutton"><a href="index.php?page=userpanel">Panel użytkownika</a></li>    

                    <?php
                } else {
                    ?>
                    <li class="navbutton"><a href="index.php?page=login" id="Log_in" name="Log_in">Zaloguj / Rejestracja</a></li>
                        <?php
                    }
                    ?>

            </ul> 
        </form>
    </div>
</nav>