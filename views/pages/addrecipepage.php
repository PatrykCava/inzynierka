<section class="row">
    <form method="post" id="new_recipe">
        <article class="col-xs-12">
            <div class="row">
                <div class="col-xs-6 m-b-10">
                    <label>Nazwa przepisu</label>
                    <input type="text" name="recipe_name" class="form-control">
                </div>
                <div class="col-xs-6 m-b-10">
                    <label>Opis przepisu</label>
                    <input type="text" name="recipe_discription" class="form-control">
                </div>
                <div class="col-xs-12 m-b-10">
                    <label>Przygotowanie</label>
                    <input type="text" name="how_to_make" class="form-control">
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-12">
                    <label>Składniki</label>
                </div>
                <div class="ingredient">
                    <div class="row">
                        <div class="col-xs-12 m-b-10">
                            <div class="col-xs-4">
                                <label>Ile?</label>
                                <input class="how_much form-control" type="number" name="howMuch[]" value="1" step="0.1" min="0.1" onchange="checkValue()">
                            </div>
                            <div class="col-xs-4">
                                <label>Miarka: </label>
                                <select class="form-control type_select" name="type[]" onchange="insertIngrediant(1)">
                                    <?php
                                    foreach ($allUnits as $eachUnit) {
                                        ?>
                                        <option value="<?php echo $eachUnit['id_jednostka'] ?>"><?php echo $eachUnit['nazwa_jednostki'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-xs-4">
                                <label>Jaki?</label>
                                <select id="ingredient_select_1" class="form-control ingredient_select" name="ingredient[]">
                                    <script>firstFill(1)</script>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4"></div>
                <div class="col-xs-4">
                    <button class="btn btn-info m-b-10 form-control" type="button" id="add_next_ingredient">następny składnik</button>                   
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-12 m-b-10" id="add_img">
                    <label>Zdjęcia wypieku:</label>
                    <input class="form-control" type="file" name="img[]" multiple>
                </div>
                <div class="col-xs-12 m-b-20">
                    <button class="btn btn-success form-control" type="button" name="add_recipe" onclick="addRecipe()">Dodaj przepis</button>
                </div>
        </article>
    </form>
</section>