<div class="row">
    <section class="col-xs-12">
        <form method="post">
            <article class="col-xs-6">
                <div class=" square loginSquare">
                    <h3 class="p-t-10">Zaloguj się na swoje konto.</h3>
                    <p>Login:</p>
                    <input type="text" name="login">
                    <p>Haslo:</p>
                    <input type="password" name="password">
                    <div class="p-b-10 p-t-10">
                        <button name="log_in" type="submit">Zaloguj się</button>
                        <a>Zapomniałeś hasła?</a>
                    </div>
                </div>
            </article>
        </form>
        <form method="post">
            <article class="col-xs-6 square registerSquare">
                <h3 class="m-t-10">Nie masz jeszcze konta? Załóż je!</h3>
                <div class="col-xs-12 center">
                    <p class="col-xs-3">Imię:</p>
                    <input class="col-xs-9" type="text" name="first_name">
                </div>
                <div class="col-xs-12">
                    <p class="col-xs-3">Nazwisko:</p>
                    <input class="col-xs-9" type="text" name="last_name">
                </div>
                <div class="col-xs-12">
                    <p class="col-xs-3">Login:</p>
                    <input class="col-xs-9" type="text" name="loginRegister">
                </div>
                <div class="col-xs-12">
                    <p class="col-xs-3">Hasło:</p>
                    <input class="col-xs-9" type="password" name="passwordRegister">
                </div>
                <div class="col-xs-12">
                    <p class="col-xs-3">Powtórz hasło:</p>
                    <input class="col-xs-9" type="password" name="repeatPasswordRegister">
                </div>
                <div class="col-xs-12">
                    <p class="col-xs-3">E-mail:</p>
                    <input class="col-xs-9" type="text" name="email">
                </div>
                <div class="col-xs-12">
                    <input class="col-xs-1" type="checkbox" name="acceptRulez">
                    <p class="col-xs-11">Akceptujesz regulamin?</p>
                </div>
                <button class="col-xs-12 m-b-10" name="create_account" type="submit">Stwórz konto</button>
            </article>
        </form>
    </section>
</div>
