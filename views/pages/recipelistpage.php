<section class="m-b-10">
    <form method="post">
        <article class="col-xs-12">
            <div class="col-xs-9">
                <p class="filter">Ilość przepisów: </p>
                <select class="filter" name="limitpage">
                    <?php
                    foreach ($select as $element) {
                        if ($_POST['limitpage'] == $element || $_SESSION['limitpage'] == $element) {
                            echo '<option value=' . $element . ' selected>' . $element . '</option>';
                        } else {
                            echo '<option value=' . $element . '>' . $element . '</option>';
                        }
                    }
                    ?>
                </select>
                <p class="filter">Szukaj po nazwie: </p>
                <input class="filter" type="text" name="searchrecipe">
                <button class="filter" name="accChangesRecipe" type="submit">Zatwierdź filtry</button>
            </div>
            <div class="col-xs-3">
                <button name="all_recipe" type="submit">Wyświetl wszystkie</button>
            </div>
        </article>
    </form>
</section>

<section class="row">
    <form method="post">
        <?php
        if (!empty($allRecipe)) {
            foreach ($allRecipe as $eachRecipe) {
                ?>
                <article class="col-xs-12 recipe m-b-10">
                    <div>
                        <div class="col-xs-3">
                            <?php
                            if (!empty(getImgSrc($eachRecipe['id_przepisu']))) {
                                ?>
                                <img class="m-t-10 image m-b-10" src="<?php echo getImgSrc($eachRecipe['id_przepisu']) ?>">
                                <?php
                            }
                            ?>
                        </div>
                        <div class="col-xs-6 recipe_name recipe_padding"><p><?php echo $eachRecipe['nazwa_przepisu'] ?></p></div>
                        <div class="col-xs-3"><button value="<?php echo $eachRecipe['id_przepisu'] ?>" name="more_detail">Zobacz więcej</button></div>
                    </div>
                </article>
                <?php
            }
        } else {
            ?>
            <p>Nie ma takiego przepisu</p>
            <?php
        }
        ?>
    </form>
</section>


<?php
if ($numberOfPages != 1 && (empty($_POST['searchrecipe']) || $_POST['searchrecipe'] == "")) {
    ?>
    <section class="col-xs-12 m-b-10">
        <article class="row">
            <div class="col-xs-1"></div>
            <div class="col-xs-2">
                <p>Następna</p>
            </div>

            <div class="col-xs-1"></div>
            <div class="col-xs-4">
                <?php
                for ($index = 1; $index <= $numberOfPages; $index++) {
                    ?>
                    <a href="index.php?page=recipelist&nrpage=<?php echo $index; ?>"><?php echo $index; ?></a>
                    <?php
                }
                ?>
            </div>
            <div class="col-xs-1"></div>
            <div class="col-xs-2">
                <p>Poprzednia</p>
            </div>
            <div class="col-xs-1"></div>
        </article>
    </section>
    <?php
}
?>