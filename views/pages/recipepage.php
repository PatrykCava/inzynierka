<section class="row">
    <article class="col-xs-12 recipe">

        <div>
            <img>
            <h3><?php echo $recipeInfo[0]['nazwa_przepisu'] ?></h3>
        </div>
        <hr>
        <p>Skladniki:</p>
        <ul>
            <?php
            getIngredients($recipeInfo);
            ?>
        </ul>
        <hr>
        <p><?php echo $recipeInfo[0]['przygotowanie_przepisu'] ?></p>

        <?php
        $images = getImages($recipeInfo);
        
        if (!empty($images) && count($images) > 1) {
            ?>
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <?php
                    for ($i = 0; $i < count($images); $i++) {
                        ?>
                        <li data-target="#myCarousel" data-slide-to="<?php echo $i ?>" <?php if ($i == 0) { ?> class="active" <?php } ?>></li>
                        <?php
                    }
                    ?>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">

                    <?php
                    for ($i = 0; $i < count($images); $i++) {
                        $arrayAlt = explode('/', $images[$i]);
                        ?>

                        <div class="item <?php if ($i == 0) { ?> active <?php } ?>">
                            <img src="<?php echo $images[$i] ?>" alt="<?php echo $arrayAlt[2] ?>" style="width:100%;">
                        </div>

                    <?php } ?>

                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <?php
        } elseif (!empty($images) && count($images) == 1) {
            $arrayAlt = explode('/', $images[0]);
            ?>
        <div>
            <img src="<?php echo $images[0] ?>" alt="<?php echo $arrayAlt[2] ?>">
        </div>
            <?php
        }
        ?>
    </article> 
</section>