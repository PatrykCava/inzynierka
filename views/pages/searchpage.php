<section class="row">
    <article class="col-xs-6">
        <div class="roundbutton" id="convert" onclick="pageConvert()">
            <p>Przelicz</p>
        </div>
    </article>

    <article class="col-xs-6">
        <div class="roundbutton" id="recipeElement" onclick="pageRecipeElement()">
            <p>Szukaj po składnikach</p>
        </div>
    </article>
    
    <article class="col-xs-6">
        <div class="roundbutton" id="peopleCount" onclick="pageCountPeople()">
            <p>Szukaj po ilości osób</p>
        </div>
    </article>
</section>